# frozen_string_literal: true

# --- Day 1: Calorie Counting ---
#
# Santa's reindeer typically eat regular reindeer food, but they need a lot of magical energy to deliver presents on Christmas. For that, their favorite snack is a special type of star fruit that only grows deep in the jungle. The Elves have brought you on their annual expedition to the grove where the fruit grows.
#
# To supply enough magical energy, the expedition needs to retrieve a minimum of fifty stars by December 25th. Although the Elves assure you that the grove has plenty of fruit, you decide to grab any fruit you see along the way, just in case.
#
# Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!
#
# The jungle must be too overgrown and difficult to navigate in vehicles or access from the air; the Elves' expedition traditionally goes on foot. As your boats approach land, the Elves begin taking inventory of their supplies. One important consideration is food - in particular, the number of Calories each Elf is carrying (your puzzle input).
#
# The Elves take turns writing down the number of Calories contained by the various meals, snacks, rations, etc. that they've brought with them, one item per line. Each Elf separates their own inventory from the previous Elf's inventory (if any) by a blank line.
#
# For example, suppose the Elves finish writing their items' Calories and end up with the following list:
#
# 1000
# 2000
# 3000
#
# 4000
#
# 5000
# 6000
#
# 7000
# 8000
# 9000
#
# 10000
#
# This list represents the Calories of the food carried by five Elves:
#
#     The first Elf is carrying food with 1000, 2000, and 3000 Calories, a total of 6000 Calories.
#     The second Elf is carrying one food item with 4000 Calories.
#     The third Elf is carrying food with 5000 and 6000 Calories, a total of 11000 Calories.
#     The fourth Elf is carrying food with 7000, 8000, and 9000 Calories, a total of 24000 Calories.
#     The fifth Elf is carrying one food item with 10000 Calories.
#
# In case the Elves get hungry and need extra snacks, they need to know which Elf to ask: they'd like to know how many Calories are being carried by the Elf carrying the most Calories. In the example above, this is 24000 (carried by the fourth Elf).
#
# Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?

elf_map = {}

filename = "day_1.txt"
elf_number = 1
total = 0
File.foreach(filename).with_index do |line|
  if line.strip.empty?
    elf_map[elf_number] = total
    total = 0
    elf_number += 1
  else
    total += line.to_i
  end
end

def get_top_elf(elf_map)
  elf_map.sort_by {|_key, value| value}.reverse[0]
end

def top_n_elves(elf_map, top_n)
  sorted = elf_map.sort_by {|_key, value| value}.reverse
  output = {}
  top_n.times do |index|
    output.store(sorted[index][0], sorted[index][1])
  end
  output
end

top_elf = get_top_elf(elf_map)
puts "heaviest elf: #{top_elf[0]} calories: #{top_elf[1]}"

top_3 = top_n_elves(elf_map, 3)
puts "top 3 elves: #{top_3}"

puts "top 3 sum: #{top_3.sum {|_,v| v }}"

