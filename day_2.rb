# frozen_string_literal: true

# --- Day 2: Rock Paper Scissors ---
#
# The Elves begin to set up camp on the beach. To decide whose tent gets to be closest to the snack storage, a giant Rock Paper Scissors tournament is already in progress.
#
# Rock Paper Scissors is a game between two players. Each game contains many rounds; in each round, the players each simultaneously choose one of Rock, Paper, or Scissors using a hand shape. Then, a winner for that round is selected: Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock. If both players choose the same shape, the round instead ends in a draw.
#
# Appreciative of your help yesterday, one Elf gives you an encrypted strategy guide (your puzzle input) that they say will be sure to help you win. "The first column is what your opponent is going to play: A for Rock, B for Paper, and C for Scissors. The second column--" Suddenly, the Elf is called away to help with someone's tent.
#
# The second column, you reason, must be what you should play in response: X for Rock, Y for Paper, and Z for Scissors. Winning every time would be suspicious, so the responses must have been carefully chosen.
#
# The winner of the whole tournament is the player with the highest score. Your total score is the sum of your scores for each round. The score for a single round is the score for the shape you selected (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round (0 if you lost, 3 if the round was a draw, and 6 if you won).
#
# Since you can't be sure if the Elf is trying to help you or trick you, you should calculate the score you would get if you were to follow the strategy guide.
#
# For example, suppose you were given the following strategy guide:
#
# A Y
# B X
# C Z
#
# This strategy guide predicts and recommends the following:
#
#     In the first round, your opponent will choose Rock (A), and you should choose Paper (Y). This ends in a win for you with a score of 8 (2 because you chose Paper + 6 because you won).
#     In the second round, your opponent will choose Paper (B), and you should choose Rock (X). This ends in a loss for you with a score of 1 (1 + 0).
#     The third round is a draw with both players choosing Scissors, giving you a score of 3 + 3 = 6.
#
# In this example, if you were to follow the strategy guide, you would get a total score of 15 (8 + 1 + 6).
#
# What would your total score be if everything goes exactly according to your strategy guide?

class Rock
  attr identifier_first = 'A'
  attr identifier_second = 'X'
  def value
    1
  end
  def defeats
    Scissors
  end
  def defeated_by
    Paper
  end
end

class Paper
  attr identifier_first = 'B'
  attr identifier_second = 'Y'
  def value
    2
  end
  def defeats
    Rock
  end
  def defeated_by
    Scissors
  end
end

class Scissors
  attr identifier_first = 'C'
  attr identifier_second = 'Z'
  def value
    3
  end
  def defeats
    Paper
  end
  def defeated_by
    Rock
  end
end

OUTCOME_WIN = 6
OUTCOME_LOSE = 0
OUTCOME_DRAW = 3


def determine_outcome_part_1(first, second)
  if first == 'A'
    first_choice = Rock.new
  elsif first == 'B'
    first_choice = Paper.new
  elsif first == 'C'
    first_choice = Scissors.new
  else
    throw StandardError "#{first} is not an allowed value!"
  end

  if second == 'X'
    second_choice = Rock.new
  elsif second == 'Y'
    second_choice = Paper.new
  elsif second == 'Z'
    second_choice = Scissors.new
  else
    raise StandardError.new "#{second} is not an allowed value!"
  end

  if first_choice.defeats == second_choice.class
    outcome = OUTCOME_LOSE
  elsif second_choice.defeats == first_choice.class
    outcome = OUTCOME_WIN
  elsif second_choice.class == first_choice.class
    outcome = OUTCOME_DRAW
  else
    raise StandardError.new "Unknown result! #{first_choice}, #{second_choice}"
  end
  outcome + second_choice.value
end

def determine_outcome_part_2(first, second)
  if first == 'A'
    first_choice = Rock.new
  elsif first == 'B'
    first_choice = Paper.new
  elsif first == 'C'
    first_choice = Scissors.new
  else
    throw StandardError "#{first} is not an allowed value!"
  end

  # Second is now how the round should end; X is lose, Y is draw, Z is win
  if second == 'X'
    second_choice = first_choice.defeats.new
  elsif second == 'Y'
    second_choice = first_choice.class.new
  elsif second == 'Z'
    second_choice = first_choice.defeated_by.new
  else
    raise StandardError.new "#{second} is not an allowed value!"
  end

  if first_choice.defeats == second_choice.class
    outcome = OUTCOME_LOSE
  elsif second_choice.defeats == first_choice.class
    outcome = OUTCOME_WIN
  elsif second_choice.class == first_choice.class
    outcome = OUTCOME_DRAW
  else
    raise StandardError.new "Unknown result! #{first_choice}, #{second_choice}"
  end
  outcome + second_choice.value
end

def read_strategy_guide(filename)
  total = 0
  File.foreach(filename).with_index do |line|
    unless line.strip.empty?
      values = line.split(" ")
      total += determine_outcome_part_2(values[0], values[1])
    end
  end
  total
end

filename = "day_2_input.txt"
final_value = read_strategy_guide(filename)

puts "game score: #{final_value}"
