# frozen_string_literal: true

@alphabet = %w[a b c d e f g h i j k l m n o p q r s t u v w x y z]

def is_upper?(character)
  character == character.upcase
end

def get_priority(character)
  index = @alphabet.index(character.downcase) + 1
  if is_upper?(character)
    index + 26
  else
    index
  end
end

def find_duplicate_character(first_half, second_half)
  first_half.chars.each do |character|
    return character unless second_half.index(character).nil?
  end
end

def find_duplicate_character_three(first_half, second_half, third_string)
  first_half.chars.each do |character|
    if !second_half.index(character).nil?
      if !third_string.index(character).nil?
        return character
      end
    end
  end
end

def find_individual_elf_duplicate(line)
  first_half = line[0...(line.length / 2)]
  second_half = line[(line.length / 2)...(line.length - 1)]

  character = find_duplicate_character(first_half, second_half)
  priority = get_priority(character)
  puts "line: #{line}   first_half: #{first_half}   second_half: #{second_half}  duplicate_character: #{character} priority: #{priority}"

  priority
end


# filename = 'day_3.txt'
filename = 'day_3_pt2.txt'
total_priority = 0
group = []
File.foreach(filename) do |line|
  if group.length == 3
    badge = find_duplicate_character_three(group[0], group[1], group[2])
    priority = get_priority(badge)
    total_priority += priority
    puts "badge: #{badge} priority: #{priority}"
    group = [].append(line)
  else
    group.append(line)
  end

  # priority = find_individual_elf_duplicate(line)
  # total_priority += priority
end
puts "total_priority: #{total_priority}"


