# frozen_string_literal: true

# --- Day 5: Supply Stacks ---
#
# The expedition can depart as soon as the final supplies have been unloaded from the ships. Supplies are stored in stacks of marked crates, but because the needed supplies are buried under many other crates, the crates need to be rearranged.
#
# The ship has a giant cargo crane capable of moving crates between stacks. To ensure none of the crates get crushed or fall over, the crane operator will rearrange them in a series of carefully-planned steps. After the crates are rearranged, the desired crates will be at the top of each stack.
#
# The Elves don't want to interrupt the crane operator during this delicate procedure, but they forgot to ask her which crate will end up where, and they want to be ready to unload them as soon as possible so they can embark.
#
# They do, however, have a drawing of the starting stacks of crates and the rearrangement procedure (your puzzle input). For example:
#
#     [D]
# [N] [C]
# [Z] [M] [P]
#  1   2   3
#
# move 1 from 2 to 1
# move 3 from 1 to 3
# move 2 from 2 to 1
# move 1 from 1 to 2
#
# In this example, there are three stacks of crates. Stack 1 contains two crates: crate Z is on the bottom, and crate N is on top. Stack 2 contains three crates; from bottom to top, they are crates M, C, and D. Finally, stack 3 contains a single crate, P.
#
# Then, the rearrangement procedure is given. In each step of the procedure, a quantity of crates is moved from one stack to a different stack. In the first step of the above rearrangement procedure, one crate is moved from stack 2 to stack 1, resulting in this configuration:
#
# [D]
# [N] [C]
# [Z] [M] [P]
#  1   2   3
#
# In the second step, three crates are moved from stack 1 to stack 3. Crates are moved one at a time, so the first crate to be moved (D) ends up below the second and third crates:
#
#         [Z]
#         [N]
#     [C] [D]
#     [M] [P]
#  1   2   3
#
# Then, both crates are moved from stack 2 to stack 1. Again, because crates are moved one at a time, crate C ends up below crate M:
#
#         [Z]
#         [N]
# [M]     [D]
# [C]     [P]
#  1   2   3
#
# Finally, one crate is moved from stack 1 to stack 2:
#
#         [Z]
#         [N]
#         [D]
# [C] [M] [P]
#  1   2   3
#
# The Elves just need to know which crate will end up on top of each stack; in this example, the top crates are C in stack 1, M in stack 2, and Z in stack 3, so you should combine these together and give the Elves the message CMZ.
#
# After the rearrangement procedure completes, what crate ends up on top of each stack?

def run_instructions_on_stacks(instructions, stacks)
  instructions.each do |instruction|
    # instruction[:quantity].times do
    stacks[instruction[:to]] += (stacks[instruction[:from]].pop(instruction[:quantity]))
    # end
  end
  stacks
end

def parse_instruction(line)
  quantity = line.split(' ')[1].to_i
  from = line.split(' ')[3].to_i
  to = line.split(' ')[5].to_i
  {quantity: quantity, from: from - 1, to: to - 1} # use direct indexing
end

def parse_initial_stack(lines)
  columns = []
  lines.each do |line|
    next if line.include?(' 1   2   3')
    length = line.length
    if length > 3
      crates = (length / 3)
      crates.times do |index|
        start = index * 4
        crate = line[start..start+2]
        if crate.nil?
          next
        end
        if crate.strip.empty?
          columns[index] = [] if columns[index].nil?
        else
          letter = crate.strip.gsub('[','').gsub(']', '')
          columns[index] = [] if columns[index].nil?
          columns[index].unshift(letter) # reverses the order of the array
        end
      end
    end
  end

  columns
end

filename = 'day_5_pt_1.txt'
initial_stacks = []
all_crates = []
initial_stacks_complete = false
instructions = []
File.foreach(filename) do |line|
  initial_stacks.append(line) if !initial_stacks_complete
  if line.include?(" 1   2   3")
    initial_stacks_complete = true
    all_crates = parse_initial_stack(initial_stacks)
    next
  end

  if line.strip.empty?
    next
  end

  if initial_stacks_complete
    instructions.append(parse_instruction(line))
  end
end

last_in_each = ""
output_stacks = run_instructions_on_stacks(instructions, all_crates)
output_stacks.each do |stack|
  last_in_each += stack[stack.length - 1] if stack.length > 0
end

puts last_in_each

