# frozen_string_literal: true

#--- Day 8: Treetop Tree House ---
#
# The expedition comes across a peculiar patch of tall trees all planted carefully in a grid. The Elves explain that a previous expedition planted these trees as a reforestation effort. Now, they're curious if this would be a good location for a tree house.
#
# First, determine whether there is enough tree cover here to keep a tree house hidden. To do this, you need to count the number of trees that are visible from outside the grid when looking directly along a row or column.
#
# The Elves have already launched a quadcopter to generate a map with the height of each tree (your puzzle input). For example:
#
# 30373
# 25512
# 65332
# 33549
# 35390
#
# Each tree is represented as a single digit whose value is its height, where 0 is the shortest and 9 is the tallest.
#
# A tree is visible if all of the other trees between it and an edge of the grid are shorter than it. Only consider trees in the same row or column; that is, only look up, down, left, or right from any given tree.
#
# All of the trees around the edge of the grid are visible - since they are already on the edge, there are no trees to block the view. In this example, that only leaves the interior nine trees to consider:
#
#     The top-left 5 is visible from the left and top. (It isn't visible from the right or bottom since other trees of height 5 are in the way.)
#     The top-middle 5 is visible from the top and right.
#     The top-right 1 is not visible from any direction; for it to be visible, there would need to only be trees of height 0 between it and an edge.
#     The left-middle 5 is visible, but only from the right.
#     The center 3 is not visible from any direction; for it to be visible, there would need to be only trees of at most height 2 between it and an edge.
#     The right-middle 3 is visible from the right.
#     In the bottom row, the middle 5 is visible, but the 3 and 4 are not.
#
# With 16 trees visible on the edge and another 5 visible in the interior, a total of 21 trees are visible in this arrangement.
#
# Consider your map; how many trees are visible from outside the grid?

def taller_than_neighbors?(value, location, neighbors)
  left = neighbors.slice(0..location - 1)
  right = neighbors.slice(location + 1..neighbors.length - 1)

  left.max < value || right.max < value
end

def scenic_score(value, location, neighbors)
  if location == 0
    left = []
  else
    left = neighbors.slice(0..location - 1).reverse
  end

  if location == neighbors.length - 1
    right = []
  else
    right = neighbors.slice(location + 1..neighbors.length - 1)
  end

  left_score = 0
  left.each do |tree|
    left_score += 1
    break if tree >= value
  end

  right_score = 0
  right.each do |tree|
    right_score += 1
    break if tree >= value
  end

  { left: left_score, right: right_score }
end

def get_scenic_score(x, y, grid)
  neighbors = get_neighbors(x, y, grid)
  value = grid[y][x]
  row_score = scenic_score(value, x, neighbors[:row])
  column_score = scenic_score(value, y, neighbors[:column])
  (row_score[:left] * row_score[:right] * column_score[:left] * column_score[:right])
end

def get_column(x, grid)
  grid.map do |row|
    row[x]
  end
end

def get_neighbors(x, y, grid)
  row = grid[y]
  column = get_column(x, grid)

  {row: row, column: column}
end

def visible?(x, y, grid, length, height)
  return true if x == 0 || x == length - 1
  return true if y == 0 || y == height - 1

  neighbors = get_neighbors(x, y, grid)
  value = grid[y][x]
  return true if taller_than_neighbors?(value, y, neighbors[:column])
  return true if taller_than_neighbors?(value, x, neighbors[:row])
  false
end


filename = 'day_8.txt'
length = 0
height = 0
grid = []
File.foreach(filename) do |line|
  line.strip!
  length = line.length if length == 0
  height += 1
  row = []
  index = 0
  line.chars.each do |char|
    row[index] = char.to_i
    index += 1
  end
  grid.append(row)
end

puts "grid: #{grid}\n\nheight: #{height} length: #{length}"

visible_trees = 0
grid.each_with_index do |row, y_index|
  row.each_with_index do |tree, x_index|
    visible_tree = visible?(x_index, y_index, grid, length, height)
    visible_trees += 1 if visible_tree
  end
end

max_score = 0
grid.each_with_index do |row, y_index|
  row.each_with_index do |tree, x_index|
    score = get_scenic_score(x_index, y_index, grid)

    if score > max_score
      max_score = score
      puts "Score: #{score} x: #{x_index} y: #{y_index}"
    end
  end
end



puts "visible_trees: #{visible_trees}"